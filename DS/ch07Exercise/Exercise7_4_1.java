package ch07Exercise;

/**
 * 4.1:��д���򣬶�ֱ�Ӳ�������ϣ������ð�����򡢿�������ֱ��ѡ�����򡢶�����͹鲢������в��ԡ�
 *  @author dux
 */
import ch07.*;
import java.util.Scanner;

public class Exercise7_4_1 {

    public static void main(String[] args) throws Exception {

        int[] d = {52, 39, 67, 95, 70, 8, 25, 52};
        int[] dlta = {5, 3, 1};   //ϣ��������������
        int maxSize = 20;                  //˳����ռ��С
        SeqList L = new SeqList(maxSize); //����˳���
        for (int i = 0; i < d.length; i++) {
            RecordNode r = new RecordNode(d[i]);
            L.insert(L.length(), r);
        }

        System.out.println("����ǰ��");
        L.display();
        System.out.println("��ѡ�����򷽷���");
        System.out.println("1-ֱ�Ӳ�������");
        System.out.println("2-ϣ������");
        System.out.println("3-ð������");
        System.out.println("4-��������");
        System.out.println("5-ֱ��ѡ������");
        System.out.println("6-������");
        System.out.println("7-�鲢����");
        Scanner s = new Scanner(System.in);
        int xz = s.nextInt();
        switch (xz) {
            case 1:
                L.insertSort();
                break;            //ֱ�Ӳ�������
            case 2:
                L.shellSort(dlta);
                break;         //ϣ������
            case 3:
                L.bubbleSort();
                break;                 //ð������
            case 4:
                L.quickSort();
                break;              //��������
            case 5:
                L.selectSort();
                break;               //ֱ��ѡ������
            case 6:
                L.heapSort();       //������
                break;
            case 7:
                L.mergeSort();   //�鲢����
                break;
        }
        System.out.println("�����");
        L.display();
    }
}

//���Խ����
/*
����ǰ��
 52 39 67 95 70 8 25 52
��ѡ�����򷽷���
1-ֱ�Ӳ�������
2-ϣ������
3-ð������
4-��������
5-ֱ��ѡ������
6-������
7-�鲢����
1
�����
 8 25 39 52 52 67 70 95
 */